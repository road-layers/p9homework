package com.example.demo.pojo;

import lombok.Data;

@Data
public class Pet {
private Integer id;
private String name;
}
