package com.example.demo.pojo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class Student {
    @Value("${person.id}")
    private Integer id;
    @Value("${person.name}")
    private String name;
    @Value("${person.sex}")
    private String sex;
}
