package com.example.demo;

import com.example.demo.controller.HellowController;
import com.example.demo.pojo.MyProperties;
import com.example.demo.pojo.Person;
import com.example.demo.pojo.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

@SpringBootTest
class DemoApplicationTests {
    @Autowired
    HellowController hellowController;
    @Autowired
    Person person;
    @Autowired
    Student student;
    @Autowired
    MyProperties myProperties;
    @Autowired
    ApplicationContext applicationContext;

    @Test
    void contextLoads() {
/*
       System.out.println(hellowController.hello());
*/
        /*System.out.println(person.toString());*/
       /* System.out.println(student.toString());*/
     /*   System.out.println(myProperties.toString());*/
      boolean myService = applicationContext.containsBean("myService");
        System.out.println(myService);
    }

}
