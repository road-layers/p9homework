package com.usian;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;

@SpringBootApplication
@MapperScan(basePackages = "com.usian.mapper")
@Slf4j
@EnableScheduling
public class Day03Application {
    public static void main(String[] args) {
        SpringApplication.run(Day03Application.class,args);
        log.info("------SpringBoot启动------");
    }
@Scheduled(fixedRate = 1000)
    public static void cron() {
        log.info("------scheduling-"+new Date()+"------");
    }
}
