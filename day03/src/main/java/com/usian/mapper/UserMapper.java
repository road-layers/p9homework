package com.usian.mapper;

import com.usian.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

public interface UserMapper {
    public List<User> findAll();
}
