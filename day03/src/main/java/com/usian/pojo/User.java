package com.usian.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class User {
    private Integer id;
    private String name;
    private String age;
    private Date zrdate;
}
