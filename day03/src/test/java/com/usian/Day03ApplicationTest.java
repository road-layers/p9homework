package com.usian;

import com.usian.controller.UserController;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day03ApplicationTest {
    @Autowired
    UserController userController;

    @Test
    public void testLogger(){
        System.out.println(userController.findAll());
        Logger logger = LoggerFactory.getLogger(Day03ApplicationTest.class);
        logger.info("-------------------------------findAll");

    }
}
