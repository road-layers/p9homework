package com.usian.demo_day04;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;

@EnableScheduling
@Slf4j
@SpringBootApplication
public class DemoDay04Application {
    public static void main(String[] args) {
        SpringApplication.run(DemoDay04Application.class, args);
        log.info("----------SpringBoot启动------------");
    }

    @Scheduled(fixedRate = 1000)
    public static void cron(){
        log.info("--------------scheduling-"+new Date()+"------------");
    }
}
